let jsonfile = require('jsonfile');

const B_WORD_PATH = './lib/bWords.txt';
const C_WORD_PATH = './lib/cWords.txt';

function readJSON (path) {
    return new Promise (
        (resolve, reject) => {
            jsonfile.readFile(
                path,
                (err, obj) => {
                    if (err) reject(err);
                    else resolve(obj);
                }
            );
        }
    );
}

Promise.all([readJSON(B_WORD_PATH), readJSON(C_WORD_PATH)])
    .then(([bWords, cWords]) => {
        for (let i = 0; i < 20; i++) {
            let randomNumber1 = Math.floor(Math.random() * bWords.length);
            let randomNumber2 = Math.floor(Math.random() * cWords.length);

            console.log(`${bWords[randomNumber1]} ${cWords[randomNumber2]}`);
        }
    })
    .catch(err => console.error(err));