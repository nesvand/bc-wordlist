let fs = require('fs');
let es = require('event-stream');

const WORD_LIST_PATH = './lib/mhyph.txt';
const PRONUNCIATION_LIST_PATH = './lib/mobypron.txt';
const MERGED_MOBY_PATH = './lib/merged_moby.txt';

// LEAVING FOR REFERENCE
// const IPA_TOKENS = [
//     '&', '(@)', 'A', 'eI', '@',
//     '-', 'b', 'tS', 'd', 'E',
//     'i', 'f', 'g', 'h', 'hw',
//     'I', 'aI', 'dZ', 'k', 'l',
//     'm', 'N', 'n', 'Oi', 'AU',
//     'O', 'oU', 'u', 'U', 'p',
//     'r', 'S', 's', 'T', 'D',
//     't', '@r', 'v', 'w', 'j',
//     'Z', 'z', 'x', 'y'
// ];

/**
 * Process the Moby Hyphenated word list to generate an object that reconstructs the word and the
 * syllable count.
 *
 * @param {String} wordListPath
 * @returns {Promise.<Object, Error>}
 */
function processWordList (wordListPath) {
    return new Promise(function (resolve, reject) {
        let wordList = {};

        let wordListReadStream = fs.createReadStream(wordListPath)
            .pipe(es.split())
            .pipe(
                es.mapSync(
                    function (line) {
                        wordListReadStream.pause();

                        /*
                        * Words are listed on each line with in-word syllable breaks denoted by
                        * `�`. This means the total count of syllables for each word can be worked
                        * out by splitting along the `�` symbol, spaces and hyphens.
                        */
                        let word = line.replace(/[�]/g, '');
                        let syllables = line.split(/[� -]/).length;

                        if (word !== '') {
                            wordList[word] = {syllables};
                        }

                        wordListReadStream.resume();
                    }
                )
                    .on('error', reject)
                    .on('end', () => resolve(wordList))
            );
    });
}

/**
 * Process the Moby Pronunciation word list to generate a word list that reconstructs the word along
 * with its pronunciation.
 *
 * @param {String} pronunciationListPath
 * @returns {Promise.<Object, Error>}
 */
function processPronunciationList (pronunciationListPath) {
    return new Promise(function (resolve, reject) {
        let pronunciationList = {};

        let pronunciationListReadStream = fs.createReadStream(pronunciationListPath)
            .pipe(es.split())
            .pipe(
                es.mapSync(
                    function (line) {
                        pronunciationListReadStream.pause();

                        let [word, pronunciation] = line.split(/ /);

                        if (word.length > 0 && pronunciation.length > 0) {
                            let ipaTokens = pronunciation
                            // Split words
                                .split(/_/)
                                // Split tokens
                                .map((thisTokenString) => thisTokenString.split(/\//))
                                // Join all tokens in to a single result
                                .reduce((tokens, currTokens) => tokens.concat(currTokens), []);

                            pronunciationList[word.replace(/_/g, ' ')] = {ipaTokens};
                        }

                        pronunciationListReadStream.resume();
                    }
                )
                    .on('error', reject)
                    .on('end', ()=>resolve(pronunciationList))
            );
    });
}

Promise.all([processWordList(WORD_LIST_PATH), processPronunciationList(PRONUNCIATION_LIST_PATH)])
    .then(([wordList, pronunciationList]) => {
        let mergedWordList = {};

        for (let word in wordList) {
            if (wordList.hasOwnProperty(word) && pronunciationList[word] !== undefined) {
                mergedWordList[word] = {
                    syllables: wordList[word].syllables,
                    ipaTokens: pronunciationList[word].ipaTokens
                };
            }
        }

        return mergedWordList;
    })
    .then((jsonData) => {
        let mergedMobyWriteStream = fs.createWriteStream(MERGED_MOBY_PATH);

        mergedMobyWriteStream.write(
            JSON.stringify(jsonData),
            'utf8',
            () => console.log('json file generation complete')
        );
    });