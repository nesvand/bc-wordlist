let jsonfile = require('jsonfile');

const MERGED_MOBY_PATH = './lib/merged_moby.txt';
const C_WORD_PATH = './lib/cWords.txt';

function readJSON (path) {
    return new Promise (
        (resolve, reject) => {
            jsonfile.readFile(
                path,
                (err, obj) => {
                    if (err) reject(err);
                    else resolve(obj);
                }
            );
        }
    );
}

function writeJSON (path) {
    return function (jsonData) {
        jsonfile.writeFile(path, jsonData, err => console.error(err));
    };
}

// http://the-toast.net/2013/12/02/a-linguist-explains-the-rules-of-summoning-benedict-cumberbatch/
// http://icon.shef.ac.uk/Moby/mpron.html
function findCWords (wordList) {
    return new Promise(
        (resolve, reject) => {
            let bWords = [];

            for (let word in wordList) {
                if (wordList.hasOwnProperty(word)) {
                    if (wordList[word].syllables && wordList[word].ipaTokens) {
                        let ipaTokens = wordList[word].ipaTokens;

                        let hasThreeSyllables = wordList[word].syllables === 3;
                        let firstSyllable = ipaTokens[0];
                        let hasFirstSyllableSound = firstSyllable !== '' && firstSyllable.match(/^'(?:tS|k)/) !== null;
                        let lastSyllable = ipaTokens[ipaTokens.length - 1];
                        let hasEndSyllableSound = lastSyllable !== '' && lastSyllable.match(/(?:p|t|k|S|tS)$/) !== null;

                        if (hasThreeSyllables && hasFirstSyllableSound && hasEndSyllableSound) bWords.push(word);
                    }
                }
            }

            if (bWords.length === 0) {
                reject(new Error('no matching results'));
            }
            else {
                resolve(bWords);
            }
        }
    );
}

readJSON(MERGED_MOBY_PATH)
    .then(findCWords)
    .then(writeJSON(C_WORD_PATH))
    .then(() => console.log('success!'))
    .catch(err => console.error(err));